import React, { useEffect } from 'react';
import axios from 'axios';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Link
} from "react-router-dom";

import { initialItem, addAllItems } from './redux/action';
import RouterTable from './router/index';


function query(data){
  return new Promise((resolve, reject) => {
    setTimeout(()=>{
      resolve(data);
    }, 1000);
  });
}

function App({ initialItem, addAllItems }) {
  //const [data, setData] = useState([]);

  //從新北市政府api獲取一些資料!!!!
  useEffect(() => {
    let api = axios('https://cors-anywhere.herokuapp.com/https://data.ntpc.gov.tw/api/datasets/4DDF3F87-4D0A-4DF0-B263-FD08C913754E/json?page=0&size=100');
    //let api = axios('https://cors-anywhere.herokuapp.com/https://data.ntpc.gov.tw/api/datasets/B39EE3E2-0951-47D4-9CEF-4E646C549317/json?page=0&size=100');

    api.then(data=>{
      //setData(data);
      initialItem(data.data);
    });
  }, [initialItem]);

  return (
    <Router>
      <div className="App">
        <nav className="main-nav">
          <ul>
            <li><Link to="/">Home</Link></li>
            <li><Link to="/add">Add</Link></li>
          </ul>
        </nav>
        <RouterTable />
        <button 
          onClick={()=>{
            //新增一些自訂義的問題
            let api = query([{
              question: 'react是甚麼?',
              answer: '前端library!'
            },{
              question: 'nextjs是甚麼?',
              answer: '基於react&node的ssr框架!!'
            },{
              question: 'vue是甚麼?',
              answer: '必須要雙修的框架!'
            },{
              question: '你喜歡react嗎?',
              answer: '不! 我不喜歡!'
            }]);

            api.then(data => {
              addAllItems(data);
            });
          }}
        >
          新增一些自訂義資料
        </button>
      </div>
    </Router>
  );
}

// const mapStateToProps = (state) => {
//   return {
//     faqList: state.faqList
//   }
// };

const mapDispatchToProps = (dispatch) => {
  return {
    addAllItems: allItems => {
      dispatch(addAllItems(allItems));
    },
    initialItem: allItems => {
      dispatch(initialItem(allItems));
    }
  }
}

export default connect(null, mapDispatchToProps)(App);
