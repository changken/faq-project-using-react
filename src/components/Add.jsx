import React, {useRef} from 'react';
import { connect } from 'react-redux';
import { addItem } from '../redux/action';

function Add({ addItem }) {
    const questionRef = useRef(null);
    const answerRef = useRef(null);

    return (
        <div className="addForm">
            <h1>新增一則FAQ</h1>
            <form>
                Question: <input type="text" ref={questionRef} name="question" placeholder="Question"/><br />
                Answer: <textarea ref={answerRef} name="answer" placeholder="Answer"></textarea><br />
                <button 
                    onClick={(e)=>{
                        e.preventDefault();
                        
                        //在redux新增一則問答
                        addItem({
                            question: questionRef.current.value,
                            answer: answerRef.current.value
                        });

                        //清空輸入框
                        questionRef.current.value = '';
                        answerRef.current.value = '';
                    }}
                >
                    新增
                </button>
            </form>
        </div>
    );
}

const mapDispatchToProps = (dispatch) => {
    return {
        addItem: (item) => {
            dispatch(addItem(item));
        }
    }
};

export default connect(null, mapDispatchToProps)(Add);