import React, { useEffect, useRef } from 'react';
import { useParams } from 'react-router-dom';
import { connect } from 'react-redux';
import { editItem } from '../redux/action';

function Edit({ faqList, editItem }) {
    const { question } = useParams();

    const questionRef = useRef(null);
    const answerRef = useRef(null);

    const item = faqList.find(item => item.question === question);

    useEffect(() => {
        questionRef.current.value = item.question;
        answerRef.current.value = item.answer;
    }, []);

    return (
        <div className="editForm">
            <h1>編輯一則FAQ</h1>
            <form>
                Question: <input type="text" ref={questionRef} name="question" placeholder="Question"/><br />
                Answer: <textarea ref={answerRef} name="answer" placeholder="Answer"></textarea><br />
                <button 
                    onClick={(e)=>{
                        e.preventDefault();
                        
                        //在redux編輯一則問答
                        editItem(item, {
                            question: questionRef.current.value,
                            answer: answerRef.current.value
                        });
                    }}
                >
                    編輯
                </button>
            </form>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        faqList: state.faqList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        editItem: (oldItem, newItem) => {
            dispatch(editItem(oldItem, newItem));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Edit);