import React from 'react';
import { connect } from 'react-redux';
import { deleteItem } from '../redux/action';
import { Link } from 'react-router-dom';

function Main({ faqList, deleteItem }) {
    const renderFAQ = faqList.map(item => 
        <li key={item.question}>
            <div>
                {item.question}
            </div>
            <div>
                {item.answer}
            </div>
            <div>
                <Link to={`/edit/${item.question}`}>Edit</Link>
            </div>
            <div 
                onClick={ ()=>{
                    deleteItem(item);
                }}
            >
                Delete
            </div>
        </li>
    )

    return (
        <div>
            <h1>新北市政府政風處FAQ</h1>
            <ul className="faqList">
                {renderFAQ}
            </ul>
        </div>
    );
}

const mapStateToProps = (state) => {
    return {
        faqList: state.faqList
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        deleteItem: item => {
            dispatch(deleteItem(item));
        }
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Main);