export const ADD = 'ADD';
export const ADDALL = 'ADDALL';
export const EDIT= 'EDIT';
export const DELETE = 'DELETE';
export const INIT = 'INIT';

export const addItem = item => {
    return {
        type: ADD,
        payload: item
    };
};

export const addAllItems = allItems => {
    return {
        type: ADDALL,
        payload: allItems
    }
};

export const editItem = (oldItem, newItem) => {
    return {
        type: EDIT,
        oldPayload: oldItem,
        payload: newItem
    }
};

export const deleteItem = item => {
    return {
        type: DELETE,
        payload: item
    };
};

export const initialItem = allItems => {
    return {
        type: INIT,
        payload: allItems
    };
};