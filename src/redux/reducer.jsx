import {ADD, INIT, ADDALL, DELETE, EDIT} from './action';

export default function reducer(state={}, action) {
    switch(action.type){
        case ADD:
            return {...state, faqList: [action.payload, ...state.faqList]};
        case ADDALL:
            return {...state, faqList: [...action.payload, ...state.faqList]};
        case EDIT:
            const editIndex = state.faqList.findIndex(item => item.question === action.oldPayload.question);
            state.faqList[editIndex] = action.payload;
            return {...state, faqList: [...state.faqList]};
        case DELETE:
            const faqList = state.faqList;
            const deleteIndex = state.faqList.findIndex( item => item.question === action.payload.question);
            faqList.splice(deleteIndex, 1);
            return {...state, faqList: [...faqList]};
        case INIT:
            return {...state, faqList: [...action.payload]};
        default:
            return {...state, faqList: []};
    }
};