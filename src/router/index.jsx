import React from 'react';
import {
    Switch,
    Route,
} from "react-router-dom";
import Main from '../components/Main';
import Add from '../components/Add';
import Edit from '../components/Edit';

function index(props) {
    return (
        <div>
            <Switch>
            <Route path="/add">
                <Add />
            </Route>
            <Route path="/edit/:question">
                <Edit />
            </Route>
            <Route path="/" exact={true}>
                <Main />
            </Route>
            </Switch>
        </div>
    );
}

export default index;